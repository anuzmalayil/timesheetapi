﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.contract;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return this.db.Employees.ToList();
        }
    }
}
