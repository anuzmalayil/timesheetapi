﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.contract;
using timesheet.data;
using timesheet.model;


namespace timesheet.business
{
    public class WeekService : IWeekService
    {
        public TimesheetDb db { get; }
        public WeekService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }




        public IEnumerable<Week> GetWeeks()
        {
            return this.db.Weeks;
        }
    }
}
