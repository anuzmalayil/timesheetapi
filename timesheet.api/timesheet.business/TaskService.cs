﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.contract;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
   public class TaskService : ITaskService
    {


        public TimesheetDb db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<timesheet.model.Task> GetTasks()
        {
            return this.db.Tasks;
        }

    }
}
