﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.contract;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class WeekDayService : IWeekDayService
    {
        public TimesheetDb db { get; }
        public WeekDayService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }


        public IEnumerable<WeekDay> GetWeekDays()
        {
            return this.db.WeekDays;
        }
    }
}
