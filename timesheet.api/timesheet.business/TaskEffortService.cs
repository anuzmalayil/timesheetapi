using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.contract;
using timesheet.data;
using timesheet.dto;
using timesheet.model;


namespace timesheet.business
{
    public class TaskEffortService : ITaskEffortService
    {
        public TimesheetDb db { get; }
        public TaskEffortService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }
        /// <summary>
        /// GetTaskEffortByWeek
        /// </summary>
        /// <param name="weekId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeListDto> GetTaskEffortByWeek(int weekId)
        {

            var list = new List<EmployeeListDto>();
            var employees = this.db.Employees;
            foreach (var emp in employees)
            {

                var taskeffortbyWeek = (from data in this.db.TaskEfforts where data.EmployeeId == emp.Id && data.Week.Id == weekId select data.TotalEffortHours).Sum();
                var taskeffortAverageByWeek = (from data in this.db.TaskEfforts where data.EmployeeId == emp.Id && data.Week.Id == weekId select data.TotalEffortHours).Average();
                var dto = new EmployeeListDto
                {
                    Id=emp.Id,
                    Name = emp.Name,
                    Code = emp.Code,
                    TotalWeeklyEffort = taskeffortbyWeek,
                    AverageWeeklyEffort = taskeffortAverageByWeek

                };
                list.Add(dto);


            }
            return list;
        }

        //public IEnumerable<TaskEffort> GetTaskEffortByEmployee(int employeeId)
        //{
        //    return null;
        //}
        /// <summary>
        /// GetEmployee Timesheet ByWeek
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="weekId"></param>
        /// <returns></returns>
        public TimesheetListDto GetEmployeeTimesheetByWeek(int employeeId, int weekId)
        {
           
                //total
                var dto = new TimesheetListDto
                {



                    TotalEffortOnSun = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Sun
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnMon = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.MOn
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnTue = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Tue
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnWed = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Wed
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnThu = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Thu
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnFri = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Fri
                                        select data.TotalEffortHours).Sum(),
                    TotalEffortOnSat = (from data in this.db.TaskEfforts
                                        where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Sat
                                        select data.TotalEffortHours).Sum(),


                };
                dto.WeeklyTaskEffortDtoList = new List<WeeklyTaskEffortDto>();
                //get All Tasks  done by EMployee on the week 
                var weeklyTaskIds = from data in this.db.TaskEfforts
                                    where data.Employee.Id ==employeeId && data.Week.Id == weekId
                                    select new { data.Task.Id, data.Task.Name };

                foreach (var task in weeklyTaskIds)
                {
                    dto.WeeklyTaskEffortDtoList.Add(
                        new WeeklyTaskEffortDto
                        {
                            TaskName = task.Name,
                            EffortOnSun = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Sun && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnMon = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.MOn && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnTue = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Tue && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnWed = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Wed && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnThu = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Thu && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnFri = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Fri && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault(),
                            EffortOnSat = (from data in this.db.TaskEfforts
                                           where data.Employee.Id == employeeId && data.Week.Id == weekId && data.WeekDay.Id == (int)WeekDays.Sat && data.Task.Id == task.Id
                                           select data.TotalEffortHours).FirstOrDefault()



                        });



               
                }
                return dto;
            }
            
     



        /// <summary>
        /// CreateTaskEffort
        /// </summary>
        /// <param name="effort"></param>
        /// <returns></returns>
        public bool CreateTaskEffort(EffortDto effort)
        {
            this.db.TaskEfforts.Add(new TaskEffort
            {
                EmployeeId = effort.EmployeeId,
                Description = effort.Description,
                TaskId = effort.TaskId,
                TotalEffortHours = effort.TotalEffortHours,
                WeekDayId = effort.WeekDayId,
                WeekId = effort.WeekId
            });
            this.db.SaveChanges();
            return true;
        }
        /// <summary>
        /// UpdateTaskEffort
        /// </summary>
        /// <param name="effort"></param>
        /// <returns></returns>
        public bool UpdateTaskEffort(EffortDto effort)
        {
            var taskEffort = this.db.TaskEfforts.FirstOrDefault(x => x.WeekId == effort.Id);

            if(taskEffort != null)
            {
                taskEffort.EmployeeId = effort.EmployeeId;
                taskEffort.Description = effort.Description;
                taskEffort.TaskId = effort.TaskId;
                taskEffort.TotalEffortHours = effort.TotalEffortHours;
                taskEffort.WeekDayId = effort.WeekDayId;
                taskEffort.WeekId = effort.WeekId;
            }

 
            this.db.SaveChanges();
            return true;
        }

        public IEnumerable<TaskEffort> GetAll()
        {
            return this.db.TaskEfforts.ToList();
        }
    }
}
