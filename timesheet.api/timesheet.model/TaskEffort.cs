﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public  class TaskEffort
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

     //   public int WeekDayId { get; set; }

     //   public int WeekId{ get; set; }

        public decimal TotalEffortHours { get; set; }

        [StringLength(255)]
        [Required]
        public string Description { get; set; }

        //  public ICollection<Task> Tasks { get; set; }
        public Employee Employee { get; set; }
        public Task Task { get; set; }

        public int EmployeeId { get; set; }

        public int WeekId { get; set; }

        public int WeekDayId { get; set; }

        public int TaskId { get; set; }
        // public ICollection<Employee> Employees { get; set; }

        public Week Week { get; set; }
        public WeekDay WeekDay { get; set; }
    }
}
 