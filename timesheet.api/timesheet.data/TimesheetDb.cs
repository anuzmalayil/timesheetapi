﻿using Microsoft.EntityFrameworkCore;
using System;
using timesheet.model;

namespace timesheet.data
{
    public class TimesheetDb : DbContext
    {
        public TimesheetDb(DbContextOptions<TimesheetDb> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Week> Weeks { get; set; }
        public DbSet<WeekDay> WeekDays { get; set; }
        public DbSet<TaskEffort> TaskEfforts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


          //  modelBuilder.Entity<Week>()
          //.HasOne(a => a.TaskEffort)
          //.WithOne(b => b.Week)
          //.HasForeignKey<TaskEffort>(b => b.WeekId);


        }
    }
}
