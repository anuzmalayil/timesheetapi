﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class RemoveConstraints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskEfforts_WeekDays_WeekDayId",
                table: "TaskEfforts");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskEfforts_Weeks_WeekId",
                table: "TaskEfforts");

            migrationBuilder.DropIndex(
                name: "IX_TaskEfforts_WeekId",
                table: "TaskEfforts");

            migrationBuilder.AlterColumn<int>(
                name: "WeekId",
                table: "TaskEfforts",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "WeekDayId",
                table: "TaskEfforts",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_TaskEfforts_WeekId",
                table: "TaskEfforts",
                column: "WeekId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskEfforts_WeekDays_WeekDayId",
                table: "TaskEfforts",
                column: "WeekDayId",
                principalTable: "WeekDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskEfforts_Weeks_WeekId",
                table: "TaskEfforts",
                column: "WeekId",
                principalTable: "Weeks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskEfforts_WeekDays_WeekDayId",
                table: "TaskEfforts");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskEfforts_Weeks_WeekId",
                table: "TaskEfforts");

            migrationBuilder.DropIndex(
                name: "IX_TaskEfforts_WeekId",
                table: "TaskEfforts");

            migrationBuilder.AlterColumn<int>(
                name: "WeekId",
                table: "TaskEfforts",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "WeekDayId",
                table: "TaskEfforts",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskEfforts_WeekId",
                table: "TaskEfforts",
                column: "WeekId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskEfforts_WeekDays_WeekDayId",
                table: "TaskEfforts",
                column: "WeekDayId",
                principalTable: "WeekDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskEfforts_Weeks_WeekId",
                table: "TaskEfforts",
                column: "WeekId",
                principalTable: "Weeks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
