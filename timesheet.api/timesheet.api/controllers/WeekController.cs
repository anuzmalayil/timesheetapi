﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.contract;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/week")]
    [ApiController]
    public class WeekController : Controller
    {
        private readonly IWeekService weekService;
        public WeekController(IWeekService employeeService)
        {
            this.weekService = employeeService;
        }
        /// <summary>
        /// GET All Weeks
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public IEnumerable<Week> GetAll()
        {
         
           var weeks  = this.weekService.GetWeeks();
            return weeks;
        }


    }
}