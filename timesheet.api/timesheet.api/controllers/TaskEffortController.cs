﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.contract;
using timesheet.dto;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TaskEffortController : Controller
    {
        private readonly ITaskEffortService taskEffortService;
        public TaskEffortController(ITaskEffortService taskEffortService)
        {
            this.taskEffortService = taskEffortService;
        }
        [HttpGet]
        public IEnumerable<TaskEffort> Get()
        {

            var data = this.taskEffortService.GetAll();
            return data;
        }
        
        [HttpGet("{id}/{weekId}")]
        public List<TimesheetListDto>Get(int id,int weekId)
        {

            var list = new List<TimesheetListDto>();
            var data = this.taskEffortService.GetEmployeeTimesheetByWeek(id, weekId);
            list.Add(data);
            return list;
        }

        [HttpGet("{id}")]
        public IEnumerable<EmployeeListDto> GetTaskEffortByWeek(int id)
        {

            var data = this.taskEffortService.GetTaskEffortByWeek(id);
            return data;
        }
        [HttpPost]
        public JsonResult Create(EffortDto effortDto)
        {

            var result = this.taskEffortService.CreateTaskEffort(effortDto);
            return Json(result);
        }
        [HttpPut]
        public JsonResult Update(EffortDto effortDto)
        {
            var result = this.taskEffortService.UpdateTaskEffort(effortDto);
            return Json(result);
        }
    }


}