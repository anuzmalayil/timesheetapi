﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.contract;
using timesheet.dto;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService employeeService;
        private readonly ITaskEffortService taskEffortService;
        public EmployeeController(IEmployeeService employeeService,ITaskEffortService taskEffortService)
        {
            this.employeeService = employeeService;
            this.taskEffortService = taskEffortService;
        }
        /// <summary>
        /// GET All Employees
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public IActionResult GetAll()
        {
         
           var employees  = this.employeeService.GetEmployees();
            return Ok(employees);
        }
        /// <summary>
        /// GET All GetTaskEffortByWeek
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
      

    }
}