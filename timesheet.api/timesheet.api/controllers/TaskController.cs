﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.contract;

namespace timesheet.api.controllers
{
     
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : Controller
    {
        private readonly ITaskService taskService;
        public TaskController(ITaskService taskService)
        {
            this.taskService = taskService;
        }
        /// <summary>
        /// GET ALL Tasks
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public IEnumerable<timesheet.model.Task> GetAll()
        {
            var  tasks = this.taskService.GetTasks();
            return tasks;
        }

    }
}