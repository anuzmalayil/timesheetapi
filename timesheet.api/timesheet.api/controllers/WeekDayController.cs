﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.contract;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/weekday")]
    [ApiController]
    public class WeekDayController : Controller
    {
        private readonly IWeekDayService weekDayService;
        public WeekDayController(IWeekDayService weekDayService)
        {
            this.weekDayService = weekDayService;
        }
        /// <summary>
        /// GET All Employees
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public IEnumerable<WeekDay> GetAll()
        {
         
           var data  = this.weekDayService.GetWeekDays();
            return data;
        }


    }
}