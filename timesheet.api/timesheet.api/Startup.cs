﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using timesheet.business;
using timesheet.contract;
using timesheet.data;


namespace timesheet.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(option => option.AddPolicy("timesheetAppCors", builder => {
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();

            }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<TimesheetDb>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("TimesheetDbConnection")));
         
            services.AddScoped<IEmployeeService, EmployeeService>();
 
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITaskEffortService, TaskEffortService>();
            services.AddScoped<IWeekDayService, WeekDayService>();
            services.AddScoped<IWeekService, WeekService>();
        }

        // This method gets called by the runtime. 
        // Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("timesheetAppCors");
            app.UseMvc();
        }
    }
}
        //public IConfiguration Configuration { get; }
        //public Startup(IConfiguration config)
        //{
        //    Configuration = config;
        //}

        //// This method gets called by the runtime. Use this method to add services to the container.
        //public void ConfigureServices(IServiceCollection services)
        //{
        //    services.AddCors(options =>
        //    {
        //        options.AddPolicy("CorsPolicy",
        //                          builder => builder.AllowAnyOrigin()
        //                                            .AllowAnyMethod()
        //                                            .AllowAnyHeader()
        //                                            .AllowCredentials());
        //    });

        //    services.AddDbContext<TimesheetDb>(options => 
        //            options.UseSqlServer(Configuration.GetConnectionString("TimesheetDbConnection")));

        //    // Add framework services.
          


        //    //using Dependency Injection
        //    services.AddSingleton<IEmployeeService, EmployeeService>();
        //    services.AddSingleton<ITaskService, TaskService>();
        //    services.AddSingleton<ITaskEffortService, TaskEffortService>();
        //    services.AddSingleton<IWeekDayService, WeekDayService>();
        //    services.AddSingleton<IWeekService, WeekService>();


        //}

        //// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        //{
        //    app.UseCors("CorsPolicy");
        //    //if (env.IsDevelopment())
        //    //{
        //    //    app.UseDeveloperExceptionPage();
        //    //}

        //    if (env.IsDevelopment())
        //    {
        //        app.UseDeveloperExceptionPage();
        //    }
        //    else
        //    {
        //        app.UseExceptionHandler("/Error");
        //        app.UseHsts();
        //    }

        //    app.UseHttpsRedirection();
        //    app.UseStaticFiles();
        //    app.UseCookiePolicy();

        ////    app.UseMvc();

 