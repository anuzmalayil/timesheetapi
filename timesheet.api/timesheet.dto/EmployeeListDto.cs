﻿using System;

namespace timesheet.dto
{
    public class EmployeeListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }
        public decimal TotalWeeklyEffort { get; set; }
        public decimal AverageWeeklyEffort { get; set; }
    }
}
