﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.dto
{
  public  class EffortDto
    {
        public decimal TotalEffortHours { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
        public int  EmployeeId { get; set; }
        public int WeekId { get; set; }
        public int TaskId { get; set; }
        public int WeekDayId { get; set; }

    }
}
