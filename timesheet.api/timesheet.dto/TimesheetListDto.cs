﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.dto
{
    public class TimesheetListDto
    {

        public decimal TotalEffortOnSun { get; set; }
        public decimal TotalEffortOnMon { get; set; }
        public decimal TotalEffortOnTue { get; set; }
        public decimal TotalEffortOnWed { get; set; }
        public decimal TotalEffortOnThu { get; set; }
        public decimal TotalEffortOnFri { get; set; }
        public decimal TotalEffortOnSat { get; set; }

        public List<WeeklyTaskEffortDto> WeeklyTaskEffortDtoList { get; set; }



    }
    public class WeeklyTaskEffortDto
    {
        public string TaskName { get; set; }
        public decimal EffortOnSun { get; set; }
        public decimal EffortOnMon { get; set; }
        public decimal EffortOnTue { get; set; }
        public decimal EffortOnWed { get; set; }
        public decimal EffortOnThu { get; set; }
        public decimal EffortOnFri { get; set; }
        public decimal EffortOnSat { get; set; }


    }
}
