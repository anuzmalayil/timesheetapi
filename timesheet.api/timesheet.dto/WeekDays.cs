﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.dto
{
    public enum WeekDays
    {
        Sun=0,
        MOn=1,
        Tue=2,
        Wed=3,
        Thu=4,
        Fri=5,
        Sat=6
    }
}
