﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.contract
{
    public interface IWeekDayService
    {
        IEnumerable<WeekDay> GetWeekDays();
    }
}
