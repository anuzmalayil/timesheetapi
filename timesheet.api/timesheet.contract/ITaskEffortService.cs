﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timesheet.dto;
using timesheet.model;

namespace timesheet.contract
{
    public interface ITaskEffortService
    {
        IEnumerable<TaskEffort> GetAll();

        IEnumerable<EmployeeListDto> GetTaskEffortByWeek(int weekId);

       // IEnumerable<TaskEffort> GetTaskEffortByEmployee(int employeeId);
        TimesheetListDto GetEmployeeTimesheetByWeek(int employeeId, int weekId);

        bool CreateTaskEffort(EffortDto effort);

        bool UpdateTaskEffort(EffortDto effort);
    
    }
   
 

 
}
